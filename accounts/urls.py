from django.urls import path
from accounts.views import user_login, user_logout, user_signup
from projects.views import project_list


urlpatterns = [
    path("signup/", user_signup, name="signup"),
    path("logout/", user_logout, name="logout"),
    path("", project_list, name="home"),
    path("login/", user_login, name="login"),
]
